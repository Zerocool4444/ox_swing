import java.util.*;

public class Table {
   char[][] data= {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
   private Player currentPlayer;
   private Player o;
   private Player x;
   private Player win;
   private int turn;
//   random number to select first player
   Random rd = new Random();
   
   public Table(Player o,Player x){
       this.o=o;
       this.x=x;
       if (rd.nextInt(2) + 1 == 1) {
            currentPlayer = o;
        } else {
            currentPlayer = x;
        }
        win = null;
   }
   
   public boolean setRowCol(int row,int col){
        if (data[row - 1][col - 1] == ('-')) {
            data[row - 1][col - 1] = currentPlayer.getName();
            turn++;
            return true;
        } else {
        
            return false;
        }
   }
   
    public boolean checkWin() {
        for (int i = 0; i < 3; i++) {
            if (checkRow(i)) {
                return true;
            } else if (checkCol(i)) {
                return true;
            } else if (checkX1()) {
                return true;
            } else if (checkX2()) {
                return true;
            }
        }
        return false;
    }
   public void switchTurn() {
       currentPlayer = currentPlayer == o ? x : o;
   
   }

   public boolean checkDraw() {
       if (turn == 9) {
           win = null;
           x.draw();
           o.draw();
           return true;
       }
       return false;
   }

   public Player getWinner(){
       return win;
   }
   
   public int getTurn() {
       return turn;
   }

   public Player getCurrentPlayer() {
       return currentPlayer;
   }

   public char[][] getData(){
       return data;
   }
   
   public void setWin() {
       if (win == o) {
           o.win();
           x.lose();
       }else if (win == x) {
           x.win();
           o.lose();
       }
   }
   
   public boolean checkRow(int row){
       for (int r = 0; r < 3; r++) {
            if (data[row][r] != currentPlayer.getName()) {
                return false;
            }
        }
        win = currentPlayer;
        setWin();
        return true;
   }
   
   public boolean checkCol(int col){
        for (int c = 0; c < 3; c++) {
            if (data[c][col] != currentPlayer.getName()) {
                return false;
            }
        }
        win = currentPlayer;
        setWin();
        return true;
   }
   
   public boolean checkX1(){
       for (int i = 0; i < 3; i++) {
            if (data[i][i] != currentPlayer.getName()) {
                return false;
            }
        }
        win = currentPlayer;
        setWin();
        return true;
   }
   
   public boolean checkX2(){
       for (int i = 0; i < 3; i++) {
            if (data[2 - i][0 + i] != currentPlayer.getName()) {
                return false;
            }
        }
        win = currentPlayer;
        setWin();
        return true;
   }
}
